
from ctypes import BigEndianStructure, c_ubyte, c_uint, memmove, pointer, sizeof
from ipaddress import IPv6Address
from typing import Tuple

# ##############################################
# IPv6 constants

IPV6_PACKET_TYPE = 0x86DD

IPV6_HEADER_VERSION = 0x6  # 1 nibble
IPV6_HEADER_TRAFFIC_CLASS = 0x0  # 8 bits
IPV6_HEADER_FLOW_LABEL = 0x0  # 20 bits
IPV6_HEADER_NEXT_HEADER = 0x6  # 8 bits
IPV6_HEADER_HOP_LIMIT = 0xff  # 8 bits

# ##############################################


class IPv6Header(BigEndianStructure):
    """
    RFC8200. Represents an IPv6 packet header.

    ```
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |Version| Traffic Class |           Flow Label                  |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |         Payload Length        |  Next Header  |   Hop Limit   |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    +                                                               +
    |                                                               |
    +                         Source Address                        +
    |                                                               |
    +                                                               +
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    +                                                               +
    |                                                               |
    +                      Destination Address                      +
    |                                                               |
    +                                                               +
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ```
    """
    # _pack_ = 1
    _fields_ = [
        ('version', c_uint, 4),
        ('traffic_class', c_uint, 8),
        ('flow_label', c_uint, 20),
        ('payload_length', c_uint, 16),
        ('next_header', c_uint, 8),
        ('hop_limit', c_uint, 8),
        ('source_address', c_ubyte * 16),
        ('destination_address', c_ubyte * 16),
    ]


def pack_ipv6_header(src_ipv6: str | IPv6Address, dst_ipv6: str | IPv6Address, flow_label: int, len: int = 0xf) -> IPv6Header:
    if isinstance(src_ipv6, str):
        src_addr = IPv6Address(src_ipv6).packed
    else:
        src_addr = src_ipv6.packed

    if isinstance(dst_ipv6, str):
        dst_addr = IPv6Address(dst_ipv6).packed
    else:
        dst_addr = dst_ipv6.packed

    header = IPv6Header()
    header.version = IPV6_HEADER_VERSION
    header.traffic_class = IPV6_HEADER_TRAFFIC_CLASS
    header.flow_label = flow_label
    header.payload_length = len
    header.next_header = IPV6_HEADER_NEXT_HEADER
    header.hop_limit = IPV6_HEADER_HOP_LIMIT
    header.source_address = (c_ubyte * 16)(*list(src_addr))
    header.destination_address = (c_ubyte * 16)(*list(dst_addr))
    return header


def unpack_ipv6_header(buf: bytes) -> Tuple[str, str, int, int, int]:
    def join_bytes(a: int, b: int) -> str:
        return f'{hex(a).upper()[2:]:>02}{hex(b).upper()[2:]:>02}'

    header = IPv6Header()
    memmove(pointer(header), buf, sizeof(header))
    src = header.source_address
    dst = header.destination_address
    src = ':'.join(join_bytes(a, b)
                   for a, b in zip(bytes(src)[0::2], bytes(src)[1::2]))
    dst = ':'.join(join_bytes(a, b)
                   for a, b in zip(bytes(dst)[0::2], bytes(dst)[1::2]))

    return src, dst, int(header.next_header), int(header.payload_length), int(header.flow_label)
