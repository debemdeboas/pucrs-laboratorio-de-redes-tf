

import socket
import asyncio
from eth_utils import *
from ipv6_utils import *
from socket_utils import wait_for_isn
from tcp_utils import *
from ctypes import sizeof
import hashlib
import random


async def tcp_connect(sock: socket.SocketType,
                      mac_src: List[int], mac_dst: List[int],
                      ip_src: ipaddress.IPv6Address, ip_dst: ipaddress.IPv6Address,
                      port_src: int, port_dst: int):
    # A -->|SYN| B
    # B -->|SYN,ACK| A
    # A -->|ACK| B
    isn_src = create_isn()  # Define ISN for this communication

    # Send SYN
    opts = b''

    # mss = TCPFlagHeader()
    # mss.kind = 2
    # mss.len = 4
    # mss = bytes(mss) + (65476).to_bytes(2, 'big')
    # opts += mss

    # sack = TCPFlagHeader()
    # sack.kind = 4
    # sack.len = 2
    # opts += bytes(sack)

    # tstamps = TCPFlagHeader()
    # tstamps.kind = 8
    # tstamps.len = 10
    # tstamps = bytes(tstamps) + (0x50f5820b).to_bytes(4, 'big') + bytes(4)
    # opts += tstamps

    # opts += (1).to_bytes(1, 'big')

    # wnd = TCPFlagHeader()
    # wnd.kind = 3
    # wnd.len = 3
    # wnd = bytes(wnd) + (7).to_bytes(1, 'big')
    # opts += wnd

    flow_label = int(bin(int.from_bytes(hashlib.sha256(ip_src.packed + ip_dst.packed +
                     port_src.to_bytes(2, 'big') + port_dst.to_bytes(2, 'big')).digest(), 'big'))[2:][:20], 2)

    tcp_header = pack_tcp_header(
        (ip_src, ip_dst, sizeof(IPv6Header)),
        port_src, port_dst,
        seq_num=isn_src, opts=opts,
        # Flags
        syn=1,
    )
    ipv6_header = pack_ipv6_header(ip_src, ip_dst,
                                   flow_label,
                                   sizeof(tcp_header) + len(opts))
    eth_header = pack_eth_header(mac_src, mac_dst, IPV6_PACKET_TYPE)
    sock.send(eth_header + ipv6_header + tcp_header + opts)

    # Wait for SYN/ACK
    try:
        isn_dst, flags, data = wait_for_isn(
            sock, int(tcp_header.checksum), isn_src, 0, port_src, port_dst)
    except TimeoutError:
        print(f'Timeout on port {port_dst}')
        return False

    if flags['rst']:
        return False

    # Send ACK
    opts = b''
    isn_src += 1

    tcp_header = pack_tcp_header(
        (ip_src, ip_dst, sizeof(IPv6Header)),
        port_src, port_dst,
        seq_num=isn_src, opts=opts,
        # Flags
        ack=1
    )
    ipv6_header = pack_ipv6_header(ip_src, ip_dst,
                                   flow_label,
                                   sizeof(tcp_header) + len(opts))
    sock.send(eth_header + ipv6_header + tcp_header + opts)
    return True


async def tcp_half_opening(sock: socket.SocketType,
                           mac_src: List[int], mac_dst: List[int],
                           ip_src: ipaddress.IPv6Address, ip_dst: ipaddress.IPv6Address,
                           port_src: int, port_dst: int):
    # A -->|SYN| B
    # B -->|SYN,ACK| A
    # A -->|RST| B
    isn_src = create_isn()  # Define ISN for this communication

    # Send SYN
    opts = b''

    flow_label = int(bin(int.from_bytes(hashlib.sha256(ip_src.packed + ip_dst.packed +
                     port_src.to_bytes(2, 'big') + port_dst.to_bytes(2, 'big')).digest(), 'big'))[2:][:20], 2)

    tcp_header = pack_tcp_header(
        (ip_src, ip_dst, sizeof(IPv6Header)),
        port_src, port_dst,
        seq_num=isn_src, opts=opts,
        # Flags
        syn=1,
    )
    ipv6_header = pack_ipv6_header(ip_src, ip_dst,
                                   flow_label,
                                   sizeof(tcp_header) + len(opts))
    eth_header = pack_eth_header(mac_src, mac_dst, IPV6_PACKET_TYPE)
    sock.send(eth_header + ipv6_header + tcp_header + opts)

    # Wait for SYN/ACK
    try:
        isn_dst, flags, data = wait_for_isn(
            sock, int(tcp_header.checksum), isn_src, 0, port_src, port_dst)
    except TimeoutError:
        print(f'Timeout on port {port_dst}')
        return False

    if flags['rst']:
        return False

    # Send RST
    opts = b''
    isn_src += 1

    tcp_header = pack_tcp_header(
        (ip_src, ip_dst, sizeof(IPv6Header)),
        port_src, port_dst,
        seq_num=isn_src, opts=opts,
        # Flags
        rst=1
    )
    ipv6_header = pack_ipv6_header(ip_src, ip_dst,
                                   flow_label,
                                   sizeof(tcp_header) + len(opts))
    sock.send(eth_header + ipv6_header + tcp_header + opts)
    return True


async def tcp_fin(sock: socket.SocketType,
                  mac_src: List[int], mac_dst: List[int],
                  ip_src: ipaddress.IPv6Address, ip_dst: ipaddress.IPv6Address,
                  port_src: int, port_dst: int):
    # A -->|FIN| B
    # B -->|RST| A

    isn_src = create_isn()  # Define ISN for this communication

    # Send SYN
    opts = b''

    flow_label = int(bin(int.from_bytes(hashlib.sha256(ip_src.packed + ip_dst.packed +
                     port_src.to_bytes(2, 'big') + port_dst.to_bytes(2, 'big')).digest(), 'big'))[2:][:20], 2)

    tcp_header = pack_tcp_header(
        (ip_src, ip_dst, sizeof(IPv6Header)),
        port_src, port_dst,
        seq_num=isn_src, opts=opts,
        # Flags
        fin=1,
    )
    ipv6_header = pack_ipv6_header(ip_src, ip_dst,
                                   flow_label,
                                   sizeof(tcp_header) + len(opts))
    eth_header = pack_eth_header(mac_src, mac_dst, IPV6_PACKET_TYPE)
    sock.send(eth_header + ipv6_header + tcp_header + opts)

    # Wait for RST
    try:
        isn_dst, flags, data = wait_for_isn(
            sock, int(tcp_header.checksum), isn_src, 0, port_src, port_dst)
    except TimeoutError:
        print(f'Timeout on port {port_dst}')
        return True

    if flags['rst']:
        return False
    return True


async def tcp_fin_psh_urg(sock: socket.SocketType,
                          mac_src: List[int], mac_dst: List[int],
                          ip_src: ipaddress.IPv6Address, ip_dst: ipaddress.IPv6Address,
                          port_src: int, port_dst: int):
    # A -->|FIN,PSH,URG| B
    # B -->|RST| A

    isn_src = create_isn()  # Define ISN for this communication

    # Send SYN
    opts = b''

    flow_label = int(bin(int.from_bytes(hashlib.sha256(ip_src.packed + ip_dst.packed +
                     port_src.to_bytes(2, 'big') + port_dst.to_bytes(2, 'big')).digest(), 'big'))[2:][:20], 2)

    tcp_header = pack_tcp_header(
        (ip_src, ip_dst, sizeof(IPv6Header)),
        port_src, port_dst,
        seq_num=isn_src, opts=opts,
        # Flags
        fin=1,
        psh=1,
        urg=1
    )
    ipv6_header = pack_ipv6_header(ip_src, ip_dst,
                                   flow_label,
                                   sizeof(tcp_header) + len(opts))
    eth_header = pack_eth_header(mac_src, mac_dst, IPV6_PACKET_TYPE)
    sock.send(eth_header + ipv6_header + tcp_header + opts)

    # Wait for RST
    try:
        isn_dst, flags, data = wait_for_isn(
            sock, int(tcp_header.checksum), isn_src, 0, port_src, port_dst)
    except TimeoutError:
        print(f'Timeout on port {port_dst}')
        return True

    if flags['rst']:
        return False
    return True


low = 80
high = 80
print(f'Scanning ports {low} - {high}')

interface = 'ens33'
mac_src, mac_s = get_mac_data(interface)

mac_dst = 'A4:1F:72:F5:90:41'
mac_dst = [int(d, 16) for d in mac_dst.split(':')]

src_ip = ipaddress.IPv6Address('::1')
dst_ip = ipaddress.IPv6Address('::1')  # Google


async def main():
    for port in range(low, high + 1):
        attacks = {
            'connect': False,
            'half_opening': False,
            'fin': False,
            'fin_psh_urg': False,
        }

        attacks['fin'] = await tcp_fin(create_and_bind_socket(interface), mac_src, mac_dst, src_ip, dst_ip, random.randint(1024, 65535), port_dst=port)
        attacks['fin_psh_urg'] = await tcp_fin_psh_urg(create_and_bind_socket(interface), mac_src, mac_dst, src_ip, dst_ip, random.randint(1024, 65535), port_dst=port)
        attacks['connect'] = await tcp_connect(create_and_bind_socket(interface), mac_src, mac_dst, src_ip, dst_ip, random.randint(1024, 65535), port_dst=port)
        attacks['half_opening'] = await tcp_half_opening(create_and_bind_socket(interface), mac_src, mac_dst, src_ip, dst_ip, random.randint(1024, 65535), port_dst=port)

        status = any(attacks.values())
        successful_attacks = f', '.join(name for name, res in attacks.items() if res)
        print(f'[{port}] Port {port} is {"open" if status else "closed"} on host {dst_ip} | ' +
              f'Successful attack(s): {successful_attacks}')

if __name__ == '__main__':
    print('Remember to change the interface, IP and MAC constants!')
    asyncio.run(main())
