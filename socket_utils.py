
import socket
import time

from typing import Tuple

from eth_utils import *
from ipv6_utils import *
from tcp_utils import *


TIMEOUT_SECS = 5

def wait_for_isn(sock: socket.SocketType,
                 packet_chksum: int, # Ignore the packet we just sent
                 isn_src: int,
                 isn_dst: int,
                 srcp, dstp) -> Tuple[int, Dict[str, bool], bytes]:
    timeout = time.time() + TIMEOUT_SECS
    while True:
        if time.time() > timeout:
            raise TimeoutError
        
        buf = sock.recv(ETH_FRAME_SIZE)

        nstart = 14
        _, dst_mac, packet_type = unpack_eth_header(ETHHeader(buf[:nstart]))
        if packet_type != IPV6_PACKET_TYPE:
            continue

        _, dst_ip, _class, len, flow_lbl = unpack_ipv6_header(buf[nstart:])
        nstart += sizeof(IPv6Header)
        if _class != IPV6_HEADER_NEXT_HEADER:
            continue

        src_port, dst_port, flags, seq_num, chksum, data = unpack_tcp_header(buf[nstart:])

        if src_port != dstp:
            continue

        print(src_port, dst_port, flags)
        return isn_dst, flags, data
