import socket
from ctypes import *
from typing import Dict
from ipv6_utils import IPV6_HEADER_NEXT_HEADER, IPV6_PACKET_TYPE, IPv6Header, unpack_ipv6_header
from eth_utils import create_and_bind_socket, ETH_FRAME_SIZE, ETH_CUSTOM_PROTOCOL, unpack_eth_header, ETHHeader
from tcp_utils import unpack_tcp_header


def flatten_flags(flags: dict) -> str:
    return ','.join(sorted(flag.upper() for flag, val in flags.items() if val))

ATTACK_PATTERNS = {
    # '>SYN <SYN,ACK >ACK': 'TCP connect',
    # '>SYN <SYN,ACK >RST': 'TCP half-opening',
    '>SYN >ACK': 'TCP connect',
    '>SYN >RST': 'TCP half-opening',
    '>FIN': 'TCP FIN',
    '>FIN,PSH,URG': 'FIN, PSH and URG'
}

known_hosts: Dict[str, str] = {}

sock = create_and_bind_socket('lo')
last_chcksum = 0
while True:
    buf = sock.recv(ETH_FRAME_SIZE)

    nstart = 14
    src_mac, dst_mac, packet_type = unpack_eth_header(ETHHeader(buf[:nstart]))

    if packet_type != IPV6_PACKET_TYPE:
        continue

    src_ip, dst_ip, _class, len, flow_label = unpack_ipv6_header(buf[nstart:])
    nstart += sizeof(IPv6Header)

    if _class != IPV6_HEADER_NEXT_HEADER:
        continue

    src_port, dst_port, flags, seq_num, chksum, data = unpack_tcp_header(buf[nstart:])

    dict_key = src_ip + str(flow_label)
    if dict_key in known_hosts:
        known_hosts[dict_key] += ' >' + flatten_flags(flags) 
    else:
        known_hosts[dict_key] = '>' + flatten_flags(flags)

    if known_hosts[dict_key] in ATTACK_PATTERNS:
        print(f'Detected possible attack "{ATTACK_PATTERNS[known_hosts[dict_key]]}" from host {src_ip}')
        known_hosts[dict_key] = ''

    print(src_port, dst_port, flags, seq_num, data)
    last_chcksum = chksum
